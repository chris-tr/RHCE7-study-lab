RHCE7-study-vagrant
===================

A two-nodes CentOS 7 Vagrant lab to study for the RHCE certification by Chris T.R. Tested with the VirtualBox provider of custom built box chris-tr/RHCE7-study-lab v0.1.

The included Vagrantfile, puppet scripts, documentation, and media are © Chris T.R. 2015, licensed for distribution under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/) unless otherwise specified.

RHEL and CentOS are © RedHat Corporation.

Contents
--------

 * A Vagrantfile that sets up a server and a client based on [chris-tr/RHCE7-study-lab](https://atlas.hashicorp.com/chris-tr/RHCE7-study-lab), interconnected via a private network
 * A collection of Puppet scripts to prepare the nodes for different exercises
 * My personal notes during the RH300 RHCE rapid track course
 * Pre-installed software:
   * [Puppet](https://puppetlabs.com)
   * [X2go](http://www.x2go.org)
   * [XFCE](http://www.xfce.org)

Manual
------

Get the lab sources, including the Vagrant file:

`$ git clone git@gitlab.com:chris-tr/RHCE7-study-lab.git`

To save some time, run the following to pull the source box to your ~/vagrant.d/boxes directory before firing up the lab: 

`$ vagrant box add chris-tr/RHCE7-study-lab`

Then start the lab:

`$ vagrant up`

See `man vagrant` for more commands to manipulate your nodes.

Notes and exercises
-------------------
Go to the [Notes](notes/Intro.md) section and start studying.

Resources
---------

 - 
