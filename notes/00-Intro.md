Intro to RHEL 7 Lab
===================

Start the lab
-------------

```bash
vagrant up
```

Once fired up, the lab environment will have two CentOS 7 boxes:
 - server.example.com
 - client.example.com

The boxes come preinstalled with the following packages:
 - epel-release
 - xfce (group)
 - x2goserver-xsession
 - wget, vim-common, vim-minimal, vim-enhanced, vim-X11, bash-completion, rsync, xfce4-terminal

Accessing the boxes
-------------------

### Vagrant

You can use Vagrant's built-in ssh command:
```bash
vagrant ssh server.example.com
```

```bash
vagrant ssh client.example.com
```

### X2go

X2go will give you access to fully graphical XFCE sessions to each of your boxes. Create sessions using the graphical X2goclient and call them as follows:
```console
x2goclient --session=server.example.com --hide --tray-icon
```

```console
x2goclient --session=client.example.com --hide --tray-icon
```

### CSSH

Create a [clusterSSH](https://github.com/duncs/clusterssh/wiki) session as follows:
```console
cssh -o "-F ssh-config" {client,server}.example.com
```

Reference material
------------------

 - https://www.redhat.com/en/services/training/rh300-rhce-certification-lab-rhcsa-and-rhce-exams
 - https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/pdf/System_Administrators_Guide/Red_Hat_Enterprise_Linux-7-System_Administrators_Guide-en-US.pdf
 - https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/
 - http://www.certdepot.net/rhel7-rhce-exam-objectives/
 - http://rhce.co
 - http://www.linuxexplorers.com/red-hat-certified-engineer-rhce-ex300-study-guide/

[Extras](extras.md)
